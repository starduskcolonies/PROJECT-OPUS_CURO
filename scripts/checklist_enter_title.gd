extends LineEdit

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var font = get("custom_fonts/font") as Font

signal text_confirmed(new_text)

# Called when the node enters the scene tree for the first time.
func _ready():
	set_process(false)
	get_parent().connect("ready_complete", self, "_on_ready_complete")
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _on_ready_complete():
	owner = get_parent().owner
	pass



func _on_ChecklistEnterTitle_text_changed(new_text):
	var width = font.get_string_size(new_text).x
	if rect_size.x < width:
		rect_size.x = width
	elif rect_size.x > width:
		if width >= rect_min_size.x:
			rect_size.x = width
		else:
			rect_size.x = rect_min_size.x
	pass # Replace with function body.


func _on_ChecklistEnterTitle_focus_exited():
	if check_text_is_valid(text):
		emit_signal("text_confirmed", text)
		text = ""
	pass # Replace with function body.


func _on_ChecklistEnterTitle_text_entered(new_text):
	if check_text_is_valid(new_text):
		emit_signal("text_confirmed", new_text)
		text = ""
	pass # Replace with function body.

func check_text_is_valid(new_text):
	if !new_text.replace(" ", "") == "":
		return true
	else:
		return false
	pass