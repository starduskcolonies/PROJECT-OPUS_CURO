extends Control

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

signal ready_complete
signal items_updated(item_count, items_completed)

var progress_bar_prefab = preload("res://prefabs/point_prefabs/point_checklist_progress_bar.tscn")
var item_prefab = preload("res://prefabs/point_prefabs/point_checklist_item.tscn")
export var point_properties_panel_node : NodePath
export var confirm_item_deletion_node : NodePath


export var item_count : int = 0
export var items_completed : int = 0
export var checklist_name : String = ""

var point_properties_panel
var confirm_item_deletion
onready var checklist_enter_title = find_node("ChecklistEnterTitle", true, false) as LineEdit
onready var checklist_title_button = find_node("ChecklistTitleButton", true, false) as Button
onready var checklist_title_label = find_node("ChecklistTitleLabel", true, false) as Label
onready var checklist_container = find_node("VBoxContainer", true, false) as VBoxContainer

# Called when the node enters the scene tree for the first time.
func _ready():
	filename = ""
	set_process(false)
	point_properties_panel = get_node(point_properties_panel_node)
	confirm_item_deletion = get_node(confirm_item_deletion_node)
	
	
	
	#DISABLE LATER!
#	point_properties_panel = get_parent().get_parent().get_parent().get_parent()
#	confirm_item_deletion = point_properties_panel.get_node("ConfirmItemDeletion")
	#DISABLE LATER!
	
	
	
	
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _on_creation():
	emit_signal("ready_complete")
	_on_ChecklistTitleButton_pressed()
	point_properties_panel = get_node(point_properties_panel_node)
	confirm_item_deletion = get_node(confirm_item_deletion_node)
	pass

#func _on_ready_complete():
#	owner = get_parent().get_parent().get_parent().get_parent().get_parent().owner
#	pass

func set_checklist_name(new_name):
	if checklist_name != new_name:
		checklist_name = new_name
		checklist_title_label.text = new_name
		update()
	pass

func _on_ChecklistAddItemButton_pressed():
	if item_count == 0:
		var progress_bar = progress_bar_prefab.instance()
		checklist_container.add_child(progress_bar)
		checklist_container.move_child(progress_bar, 0)
		progress_bar.owner = owner
		progress_bar._on_creation()
		rect_min_size.y += progress_bar.rect_min_size.y
		checklist_container.rect_min_size.y += progress_bar.rect_size.y
		
		var item = item_prefab.instance()
		checklist_container.add_child(item)
		checklist_container.move_child(item, 1)
		item.owner = owner
		item.confirm_item_deletion_node = confirm_item_deletion.get_path()
		item._on_creation()
		checklist_container.rect_min_size.y += item.rect_size.y
		rect_min_size.y += item.rect_size.y
		
		item_count += 1
		emit_signal("items_updated", item_count, items_completed)
		update()
	elif item_count > 0:
		var item = item_prefab.instance()
		checklist_container.add_child(item)
		checklist_container.move_child(item, checklist_container.get_child_count() - 2)
		item.owner = owner
		item.confirm_item_deletion_node = confirm_item_deletion.get_path()
		item._on_creation()
		checklist_container.rect_min_size.y += item.rect_size.y
		rect_min_size.y += item.rect_size.y
		
		item_count += 1
		emit_signal("items_updated", item_count, items_completed)
		update()
	pass # Replace with function body.

func _on_checklist_item_size_changed(size_difference):
	if int(size_difference) != 0:
		rect_min_size.y -= size_difference
		rect_size.y = rect_min_size.y
	pass

func _on_checklist_toggled(done_or_not):
	
	if done_or_not:
		if items_completed + 1 <= item_count:
			items_completed += 1
			emit_signal("items_updated", item_count, items_completed)
	else:
		if items_completed -1 >= 0:
			items_completed -= 1
			emit_signal("items_updated", item_count, items_completed)
	pass


func _on_item_deleted(was_complete):
	if was_complete:
		items_completed -= 1
	item_count -= 1
	emit_signal("items_updated", item_count, items_completed)
	pass


func _on_ChecklistEnterTitle_text_confirmed(new_text):
	checklist_enter_title.visible = false
	checklist_title_label.set_text_custom(new_text)
	checklist_title_button.visible = true
	
	pass # Replace with function body.


func _on_ChecklistTitleButton_pressed():
	checklist_enter_title.visible = true
	checklist_enter_title.placeholder_text = checklist_title_label.text
	checklist_title_button.visible = false
	checklist_enter_title.grab_focus()
	pass # Replace with function body.
