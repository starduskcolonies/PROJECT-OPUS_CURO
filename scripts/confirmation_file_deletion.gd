extends ConfirmationDialog

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var has_confirmed = false

onready var browse_projects_menu = get_parent().get_parent().find_node("BrowseProjectsMenu")
onready var input_blocker = get_parent().find_node("InputBlocker") 

# Called when the node enters the scene tree for the first time.
func _ready():
	set_process(false)
	connect("visibility_changed", self, "_on_visibility_changed")
	get_cancel().connect("pressed", self, "_cancel_pressed")
	connect("confirmed", self, "_confirmed")
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if !visible:
		if !has_confirmed:
			browse_projects_menu.delete_state = 2
		else:
			has_confirmed = false
	pass

func _on_visibility_changed():
	if visible: 
		set_process(true)
		input_blocker.visible = true
	else:
		input_blocker.visible = false
	pass

func _cancel_pressed():
	browse_projects_menu.delete_state = 2
	has_confirmed = true
	pass

func _confirmed():
	browse_projects_menu.delete_state = 1
	has_confirmed = true
	pass