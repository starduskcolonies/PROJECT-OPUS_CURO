extends Node

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var home_menu_prefab = preload("res://scenes/home_scene.tscn")
var project_prefab = preload("res://scenes/main_project_scene.tscn")
var three_d_scene_prefab = preload("res://scenes/3d_test_scene.tscn")

var current_scene_type

onready var currently_loaded_scene = get_child(0)
onready var project_file_extension = "." + SettingsManager.project_file_extension

# Called when the node enters the scene tree for the first time.
func _ready():
	get_tree().set_auto_accept_quit(false)
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _notification(what):
	if what == MainLoop.NOTIFICATION_WM_QUIT_REQUEST:
		match currently_loaded_scene.node_type:
			"home":
				get_tree().call_deferred("quit")
				pass
			"project":
				if save_project() == OK:
					get_tree().call_deferred("quit")
					pass
				pass

func load_new_project(var project_name : String = "This Is A Project", var project_icon : ImageTexture = null, var project_is_private : bool = true, var scene_type : int = 0):
	var new_project = project_prefab.instance()
	current_scene_type = scene_type
	new_project.project_name = project_name
	new_project.project_icon = project_icon
	new_project.project_is_private = project_is_private
	new_project.scene_type = scene_type
	currently_loaded_scene.queue_free()
	currently_loaded_scene = new_project
	match scene_type:
		0:
			add_child(currently_loaded_scene)
			move_child(currently_loaded_scene, 0)
		1:
			var three_d_scene = three_d_scene_prefab.instance()
			add_child(three_d_scene)
			move_child(three_d_scene, 0)
#			three_d_scene.owner = self
			three_d_scene.camera.add_child(currently_loaded_scene)
#	currently_loaded_scene.owner = self
	currently_loaded_scene.scene_manager = self
	currently_loaded_scene._on_creation()
	pass

func load_saved_project(project_resource):
	currently_loaded_scene.queue_free()
	currently_loaded_scene = project_resource.instance()
	current_scene_type = currently_loaded_scene.scene_type
	match current_scene_type:
		0:
			add_child(currently_loaded_scene)
			move_child(currently_loaded_scene, 0)
		1:
			var three_d_scene = three_d_scene_prefab.instance()
			add_child(three_d_scene)
			move_child(three_d_scene, 0)
#			three_d_scene.owner = self
			three_d_scene.camera.add_child(currently_loaded_scene)
#	currently_loaded_scene.owner = self
	currently_loaded_scene.scene_manager = self
	currently_loaded_scene._on_creation()
	pass

func save_project():
	var project_name = currently_loaded_scene.project_name
	var scene = PackedScene.new()
	var result = scene.pack(currently_loaded_scene)
	if result == OK:
		ResourceSaver.save(SettingsManager.executable_directory + SettingsManager.save_folder + "/" + project_name + project_file_extension, scene)
	return result
	pass

func goto_home_menu():
	if save_project() == OK:
		match current_scene_type:
			0:
				var new_scene = home_menu_prefab.instance()
				get_child(0).queue_free()
				currently_loaded_scene = new_scene
				add_child(new_scene)
				move_child(new_scene, 0)
#				new_scene.owner = self
			1:
				var new_scene = home_menu_prefab.instance()
				currently_loaded_scene.queue_free()
				currently_loaded_scene = new_scene
				add_child(new_scene)
				move_child(new_scene, 0)
#				new_scene.owner = self
	pass