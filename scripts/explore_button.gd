extends Button

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

onready var coming_soon := $ComingSoon as Control

# Called when the node enters the scene tree for the first time.
func _ready():
	set_process(true)
	if coming_soon.visible: coming_soon.visible = false
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if is_hovered():
		if !coming_soon.visible: coming_soon.visible = true
	else:
		if coming_soon.visible: coming_soon.visible = false
	pass