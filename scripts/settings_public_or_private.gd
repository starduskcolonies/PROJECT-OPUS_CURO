extends CheckButton

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var text_color_normal : Color = Color("ffffff")
var text_color_dark : Color = Color("a0a0a0")
var is_private : bool = true

onready var private_label := $PrivateLabel as Label
onready var public_label := $PublicLabel as Label

# Called when the node enters the scene tree for the first time.
func _ready():
	set_process(false)
	connect("toggled", self, "_on_toggled")
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _on_toggled(var button_pressed):
	# false == private
	# true == public
	if button_pressed:
		if is_private: is_private = false
		if private_label.modulate != text_color_dark: private_label.modulate = text_color_dark
		if public_label.modulate != text_color_normal: public_label.modulate = text_color_normal
	else:
		if !is_private: is_private = true
		if private_label.modulate != text_color_normal: private_label.modulate = text_color_normal
		if public_label.modulate != text_color_dark: public_label.modulate = text_color_dark
	pass