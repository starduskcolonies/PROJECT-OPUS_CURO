extends Container

# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	get_parent().connect("ready_complete", self, "_on_ready_complete")
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _on_ready_complete():
	if owner != get_parent().owner : owner = get_parent().owner
	pass

func helper_position():
	return rect_global_position
	pass