extends Popup

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var loaded_save_files : Array = []
var no_files_prefab = load("res://prefabs/no_files_detected.tscn")
var file_button_prefab = load("res://prefabs/file_button_control.tscn")
var waiting_for_confirm_delete : bool = false
var file_to_delete : String = ""
var file_node_to_delete
var delete_state : int = 0 # 0 = waiting, 1 = confirm deletion, 2 = cancel deletion

onready var scene_manager = get_node("/root/SceneManager")
onready var project_file_extension = SettingsManager.project_file_extension
onready var file_button_container = find_node("FileButtonContainer")
onready var confirm_file_deletion = get_parent().find_node("ConfirmFileDeletion")

# Called when the node enters the scene tree for the first time.
func _ready():
	set_process(false)
	connect("visibility_changed", self, "_on_visibility_changed")
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if waiting_for_confirm_delete:
		match delete_state:
			1:
				delete_file(file_to_delete, file_node_to_delete)
				file_to_delete = ""
				file_node_to_delete = null
				delete_state = 0
				waiting_for_confirm_delete = false
				load_save_files()
				set_process(false)
			2:
				file_to_delete = ""
				file_node_to_delete = null
				delete_state = 0
				waiting_for_confirm_delete = false
				set_process(false)
		pass
	pass

func _on_visibility_changed():
	if visible:
		load_save_files()
		pass
	pass

func _on_CloseButton_pressed():
	visible = false
	pass # Replace with function body.

func load_save_files():
	var directory = SettingsManager.executable_directory + SettingsManager.save_folder + "/"
	var files = list_files_in_directory(directory, project_file_extension)
	clear_file_buttons()
	if files.size() > 0:
		for i in files:
			var project_packed_scene = ResourceLoader.load(directory + i)
			var project_scene = project_packed_scene.instance()
			var new_file_button = file_button_prefab.instance()
			file_button_container.add_child(new_file_button)
			new_file_button.set_init_values(project_scene.project_name, self, directory + i)
			project_scene.free()
	else:
		file_button_container.add_child(no_files_prefab.instance())
		pass
	pass

func load_project(path):
	scene_manager.load_saved_project(ResourceLoader.load(path))
	pass

func clear_file_buttons():
	for i in file_button_container.get_children():
		i.queue_free()
	pass

func list_files_in_directory(path, file_extension):
	var files = []
	var dir = Directory.new()
	dir.open(path)
	dir.list_dir_begin()
	while true:
		var file = dir.get_next()
		if file == "":
			break
		elif not file.begins_with("."):
			if file.get_extension() == file_extension:
				files.append(file)
	dir.list_dir_end()
	return files

func ask_to_delete_file(path_to_file, file_node):
	set_process(true)
	waiting_for_confirm_delete = true
	file_to_delete = path_to_file
	confirm_file_deletion.visible = true
	file_node_to_delete = file_node
	pass

func delete_file(path_to_file, file_node):
	var dir = Directory.new()
	dir.remove(path_to_file)
	file_node.queue_free()
	pass

