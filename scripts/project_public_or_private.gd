extends Control

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var is_private : bool = false setget set_is_private

onready var lock_icon_open = $PrivateOrPublicIconOpen
onready var lock_icon_closed = $PrivateOrPublicIconClosed

# Called when the node enters the scene tree for the first time.
func _ready():
	set_process(false)
	set_is_private(is_private)
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func set_is_private(var is_true : bool):
	if is_true:
		if !lock_icon_closed.visible: lock_icon_closed.visible = true
		if lock_icon_open.visible: lock_icon_open.visible = false
	else:
		if lock_icon_closed.visible: lock_icon_closed.visible = false
		if !lock_icon_open.visible: lock_icon_open.visible = true
	pass