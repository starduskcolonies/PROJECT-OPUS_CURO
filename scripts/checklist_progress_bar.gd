extends Control

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

signal ready_complete

export var item_count : int = 0
export var items_done : int = 0
var checklist_parent

onready var progress_bar = $ProgressBar as ProgressBar

# Called when the node enters the scene tree for the first time.
func _ready():
	filename = ""
	set_process(false)
	checklist_parent = get_parent().get_parent().get_parent()
	checklist_parent.connect("items_updated", self, "_on_items_updated")
#	get_parent().get_parent().get_parent().connect("ready_complete", self, "_on_ready_complete")
	pass # Replace with function body.

func _on_creation():
	emit_signal("ready_complete")
	pass

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _on_items_updated(count, items_completed):
	item_count = count
	items_done = items_completed
	update_percentage(item_count, items_done)
	pass

func update_percentage(count, amount_complete):
#	progress_bar.max_value = count
	if count > 0:
		var progress = (float(amount_complete) / float(count)) * 100
		progress_bar.value = progress
	else:
		progress_bar.value = 0
	pass

#func _on_ready_complete():
#	owner = get_parent().get_parent().get_parent().owner
#	pass
