extends Control

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

export var setting_name : String = ""
export var setting_category: String = "system" # by default...
export var setting_key : String = ""
onready var setting_id = setting_category + "/" + setting_key
onready var setting_value : bool = SettingsManager.get_value(setting_category,setting_id)
onready var check_button := $CheckButton as CheckButton
onready var label := $Label as Label


# Button Pressed: false == On true == Off

# Called when the node enters the scene tree for the first time.
func _ready():
	name = setting_name
	label.text = setting_name
	check_button.pressed = setting_value
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_CheckButton_toggled(button_pressed):
	setting_value = button_pressed
	SettingsManager.set_value(setting_category,setting_id, setting_value)
	match setting_category:
		"system":
			OS.set(setting_key, setting_value)
			pass
		"user":
			
			pass
		"cache":
			
			pass
	pass # Replace with function body.