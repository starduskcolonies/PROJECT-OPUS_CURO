extends Spatial

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var node_type = "3d_scene"

onready var camera = $Camera
onready var animation_player = $Node/Water/AnimationPlayer

var time_to_wait_visible = 5.25 #seconds
var timer = 0.0

# Called when the node enters the scene tree for the first time.
func _ready():
#	visible = false
#	animation_player.connect("animation_finished", self, "_on_animation_finished")
#	OS.window_maximized = true
#	OS.window_size = OS.get_screen_size(OS.current_screen)
#	OS.window_borderless = true
#	OS.window_position = Vector2(0,0)
#	set_process(true)
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if timer < time_to_wait_visible:
		timer += delta
	else:
		visible = true
		animation_player.play("uv_water")
		set_process(false)
		pass
	pass

func _on_animation_finished(animation):
	visible = false
	get_tree().quit()
	pass
