extends PopupPanel

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

onready var root = get_tree().get_root().find_node("root", true, false)
onready var center_position : Vector2 = (get_viewport_rect().size / 2) - (rect_size / 2)

# Called when the node enters the scene tree for the first time.
func _ready():
	if rect_position != center_position: rect_position = center_position
	set_process(false)
	connect("visibility_changed", self, "_on_visibility_changed")
	get_viewport().connect("size_changed", self, "_on_viewport_size_changed")
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if has_focus():
		if Input.is_action_just_released("ui_accept"):
			_on_AcceptGenericName_pressed()
			pass
		elif Input.is_action_just_released("ui_cancel"):
			_on_RejectGenericName_pressed()
			pass
	pass


func _on_visibility_changed():
	if visible:
		set_process(true)
	else:
		set_process(false)
	pass

func _on_AcceptGenericName_pressed():
	root._on_AcceptGenericName_pressed()
	release_focus()
	pass # Replace with function body.


func _on_RejectGenericName_pressed():
	root._on_RejectGenericName_pressed()
	release_focus()
	pass # Replace with function body.

func _on_viewport_size_changed():
	center_position = (get_viewport_rect().size / 2) - (rect_size / 2)
	if rect_position != center_position: rect_position = center_position
	pass