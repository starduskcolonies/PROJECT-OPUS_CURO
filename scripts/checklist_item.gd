extends Control

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

signal ready_complete
signal checklist_item_size_changed(new_size)

export var confirm_item_deletion_node : NodePath

var confirm_item_deletion
var checklist_parent
var margin_bottom_pix : float = 0.0
var margin_top_pix : float = 0.0

export var is_checked : bool = false

onready var margin_y : float = margin_bottom_pix + margin_top_pix
onready var confirm_deletion_node = find_node("ConfirmDeletion", true, false)

# Called when the node enters the scene tree for the first time.
func _ready():
	filename = ""
	set_process(false)
	confirm_item_deletion = get_node(confirm_item_deletion_node)
	checklist_parent = get_parent().get_parent().get_parent()
	connect("checklist_item_size_changed", checklist_parent, "_on_checklist_item_size_changed")
#	get_parent().get_parent().get_parent().connect("ready_complete", self, "_on_ready_complete")
	pass # Replace with function body.

func _on_creation():
	emit_signal("ready_complete")
	confirm_item_deletion = get_node(confirm_item_deletion_node)
	pass

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

#func _on_ready_complete():
#	owner = get_parent().get_parent().get_parent().owner
#	pass

func on_panel_size_changed(new_size):
	var updated_size = new_size + margin_y
	if int(new_size) != 0:
		rect_min_size.y -= new_size
		rect_size.y = rect_min_size.y
		emit_signal("checklist_item_size_changed", new_size)
	pass

func _on_DeleteChecklistItemButton_pressed():
	confirm_item_deletion.queue_deletion(self)
	pass # Replace with function body.

func _on_confirm_deletion(is_true : bool):
	if is_true:
		emit_signal("checklist_item_size_changed", rect_size.y)
		checklist_parent._on_item_deleted(is_checked)
		queue_free()
		pass
	pass


func _on_CheckBox_toggled(button_pressed):
	checklist_parent._on_checklist_toggled(button_pressed)
	is_checked = button_pressed
	pass # Replace with function body.
