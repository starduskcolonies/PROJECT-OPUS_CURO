extends Popup

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

onready var scene_manager = get_parent().get_parent()
onready var name_edit = find_node("NameEdit")
onready var project_icon = find_node("ProjectIcon")
onready var public_or_private = find_node("PublicOrPrivate")
onready var icon_file_dialog = find_node("IconFileDialog")
onready var starting_scene_type = find_node("StartingSceneType")
onready var default_project_icon = preload("res://images/default_project_icon.png")
onready var center_position : Vector2 = (get_viewport_rect().size / 2) - (rect_size / 2)

var custom_icon : ImageTexture


# Called when the node enters the scene tree for the first time.
func _ready():
	set_process(false)
	connect("visibility_changed", self, "_on_visibility_changed")
	get_viewport().connect("size_changed", self, "_on_viewport_size_changed")
	pass # Replace with function body.

# If Enter is hit, accept entered information
# note, I disabled it as I thought this would be confusing to users without further tweaks, which may be unnessessary
#func _process(delta):
#	if Input.is_action_just_released("ui_accept"):
#		_on_AcceptNewProject_pressed()
#	pass
#
func _on_visibility_changed():
	update()
	pass

func _on_viewport_size_changed():
	center_position = (get_viewport_rect().size / 2) - (rect_size / 2)
	if rect_position != center_position: rect_position = center_position
	pass

func _on_CloseButton_pressed():
	visible = false
	name_edit.text = ""
	if project_icon.texture != default_project_icon : project_icon.texture = default_project_icon
	if public_or_private.pressed : public_or_private.pressed = false
	pass # Replace with function body.


func _on_ResetProjectIcon_pressed():
	if project_icon.texture != default_project_icon : project_icon.texture = default_project_icon
	pass # Replace with function body.

func _on_PickIconHover_pressed():
	icon_file_dialog.visible = true
	pass # Replace with function body.


func _on_IconFileDialog_file_selected(path):
	var tex = ImageTexture.new()
	var image = Image.new()
	image.load(path)
	tex.create_from_image(image)
	var new_icon = tex as ImageTexture
	if custom_icon != new_icon: custom_icon = new_icon
	project_icon.texture = new_icon
	pass # Replace with function body.


func _on_AcceptNewProject_pressed():
	if name_edit.text.replace(" ","") == "":
		name_edit.text = name_edit.placeholder_text
	scene_manager.load_new_project(name_edit.text, project_icon.texture, public_or_private.is_private, starting_scene_type.scene_type)
	pass # Replace with function body.
